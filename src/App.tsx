import React from 'react';
import './App.css';
import {Box, CircularProgress} from "@mui/material";
import Modal from './components/Modal';
import PhotosStore from './stores/photosStore'
import {inject, observer} from "mobx-react";
import PhotoList from './components/PhotoList';
import BasicPagination from "./components/Pagination";
import {IInjectedProps} from "./interfaces";
import ModalStore from "./stores/modalStore";
import Filter from "./components/Filter";


@inject('rootStore')
@observer
class App extends React.Component {
    private readonly photosStore: PhotosStore;
    private modalStore: ModalStore;

    constructor(props) {
        super(props);
        const {rootStore: {photosStore, modalStore}} = this.props as IInjectedProps;
        this.photosStore = photosStore;
        this.modalStore = modalStore;
    }

    render() {
        const {
            isLoading
        } = this.photosStore;
        const {
            changeVisibility,
            isVisible,
            modalContent
        } = this.modalStore;

        return (<Box sx={{width: '100%'}} className="app">
                <div className='loader'>
                    {isLoading ? <CircularProgress/> : null}
                </div>

                <div className='top-controls'>
                    <BasicPagination/>
                    <Filter/>
                </div>

                <PhotoList/>
                <Modal changeVisibility={changeVisibility} isVisible={isVisible} modalContent={modalContent}/>
            </Box>
        );
    }

}

export default App;
