import * as React from 'react';
import MuiModal from '@mui/material/Modal';

interface IProps {
    changeVisibility: (state: boolean) => void;
    isVisible: boolean;
    modalContent: string;
}

export default function Modal(props: IProps) {
    return (
        <div>
            <MuiModal
                classes={{ root: 'modal'}}
                open={props.isVisible}
                onClose={() => props.changeVisibility(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description">
                    <img src={props.modalContent} alt=""></img>
            </MuiModal>
        </div>
    );
}