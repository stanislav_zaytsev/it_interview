import React from 'react';
import '../App.css'
import {inject, observer} from "mobx-react";
import {RootStore} from "../stores/rootStore";
import PhotosStore from "../stores/photosStore";
import {Grid, Paper, styled} from "@mui/material";
import DeleteButton from "./DeleteButton";
import {IReactionDisposer, observable, reaction} from "mobx";
import PagingStore from "../stores/pagingStore";
import ModalStore from "../stores/modalStore";
import {IPhoto} from "../interfaces";
import AlbumStore from "../stores/albumStore";

const Item = styled(Paper)(({theme}) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

interface IInjectedProps {
    rootStore: RootStore;
}


@inject('rootStore')
@observer
class PhotoList extends React.Component {
    private readonly photosStore: PhotosStore;
    private readonly pagingStore: PagingStore;
    private reactionDisposer: IReactionDisposer;
    private modalStore: ModalStore;
    private albumStore: AlbumStore;

    constructor(props) {
        super(props);
        const {rootStore: {photosStore, pagingStore, modalStore, albumStore}} = this.props as IInjectedProps;
        this.photosStore = photosStore;
        this.pagingStore = pagingStore;
        this.modalStore = modalStore;
        this.albumStore = albumStore;
    }

    @observable photos: null | IPhoto[];

    componentDidMount() {
        this.reactionDisposer = reaction(
            () => this.pagingStore.currentPage,
            this.photosStore.loadPhotos,
            {fireImmediately: true}
        );
    }

    componentWillUnmount() {
        this.reactionDisposer?.();
    }

    removePhoto(photo: IPhoto) {
        this.photosStore.removePhoto(+photo.id)
    }

    openPhoto(photo: IPhoto) {
        this.modalStore.setModalContent(photo.url);
        this.modalStore.changeVisibility(true);
    }

    render() {
        const {
            photos,
            removedPhotos
        } = this.photosStore;

        const {
            albumId
        } = this.albumStore;

        if (!photos) return 'No photos'

        let filteredPhotos = photos.filter(p => !removedPhotos.includes(+p.id))

        if (albumId) {
            filteredPhotos = photos.filter(p => p.id === albumId);
        }

        if (!filteredPhotos || !filteredPhotos.length) {
            return (
                    <h1 className='loader'>No photos</h1>
            )
        }

        return (
            <Grid container spacing={3} className="photos-list">
                {filteredPhotos.map((photo) => (
                    <Grid item xs key={photo.id}>
                        <Item className="photo">
                            <DeleteButton onClick={() => this.removePhoto(photo)}/>
                            <img src={photo.thumbnailUrl} alt="" onClick={() => this.openPhoto(photo)}/>
                            <div className="photo-name">{photo.title}</div>
                        </Item>
                    </Grid>
                ))}
            </Grid>

        );
    }
}

export default PhotoList;
