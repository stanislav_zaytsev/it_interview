import * as React from 'react';
import Pagination from '@mui/material/Pagination';
import {IInjectedProps} from "../interfaces";
import {inject, observer} from "mobx-react";
import PagingStore from "../stores/pagingStore";
import {Container, Stack} from '@mui/material';

@inject('rootStore')
@observer
export default class BasicPagination extends React.Component<any, any> {
    private readonly pagingStore: PagingStore;

    constructor(props) {
        super(props);
        const {rootStore: {pagingStore}} = this.props as IInjectedProps;
        this.pagingStore = pagingStore;
    }

    onChange = (event: React.ChangeEvent<unknown>, page: number) => {
        const {rootStore: {pagingStore}} = this.props as IInjectedProps;
        pagingStore.changePage(page);
    }

    render() {
        const {
            pagesCount
        } = this.pagingStore;

        return (
            <Stack spacing={2}>
                <Container fixed>
                    <Pagination count={pagesCount} onChange={this.onChange}/>
                </Container>
            </Stack>
        );
    }
}