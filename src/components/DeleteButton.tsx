import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';

export default function DeleteButton({onClick}) {
    return (
        <Tooltip title="Delete" className="hide-button">
            <IconButton onClick={onClick} >
                x
            </IconButton>
        </Tooltip>
    );
}