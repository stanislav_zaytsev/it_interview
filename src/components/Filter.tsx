import * as React from 'react';
import {IInjectedProps} from "../interfaces";
import {inject, observer} from "mobx-react";
import PagingStore from "../stores/pagingStore";
import {FormControl, MenuItem, Select, SelectChangeEvent} from "@mui/material";
import AlbumStore from "../stores/albumStore";
import {ChangeEvent} from "react";

@inject('rootStore')
@observer
export default class Filter extends React.Component<any, any> {
    private readonly pagingStore: PagingStore;
    private albumStore: AlbumStore;

    constructor(props) {
        super(props);
        const {rootStore: {pagingStore, albumStore}} = this.props as IInjectedProps;
        this.pagingStore = pagingStore;
        this.albumStore = albumStore;
    }

    componentDidMount() {
        this.albumStore.loadAlbums();
    }

    onChange = (event: React.ChangeEvent<unknown>, page: number) => {
        const {rootStore: {pagingStore}} = this.props as IInjectedProps;
        pagingStore.changePage(page);
    }

    changeAlbum = (event: SelectChangeEvent<ChangeEvent<HTMLInputElement>>) => {
        this.albumStore.changeAlbum(event.target.value)
    }

    render() {
        const {
            albumId,
            albums = []
        } = this.albumStore;

        return (
            <div className='filter'>
                <h2>Select album id to filter photos</h2>
                <FormControl sx={{m: 1, minWidth: 80}}>
                    <Select
                        labelId="demo-simple-select-autowidth-label"
                        id="demo-simple-select-autowidth"
                        // @ts-ignore
                        value={albumId}
                        onChange={this.changeAlbum}
                        autoWidth
                        label="Age"
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {
                            albums.map(album => (
                                <MenuItem key={album.id} value={album.id}>{album.id}</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>
            </div>

        );
    }
}