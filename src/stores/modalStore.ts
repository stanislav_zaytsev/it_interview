import {action, observable} from "mobx";
import {IPhoto} from "../interfaces";
import {loadAlbumData} from "../utils/dataActions";
import {RootStore} from "./rootStore";
import {ReactNode} from "react";

export class ModalStore {
    private rootStore: RootStore;

    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    @observable isVisible: boolean = false;
    @observable modalContent: string = '';

    @action
    setModalContent = (content: string) => {
        this.modalContent = content;
    }

    @action
    changeVisibility = (state: boolean) => {
        this.isVisible = state;
    }
}

export default ModalStore;