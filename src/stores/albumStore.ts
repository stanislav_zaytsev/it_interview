import {action, observable} from "mobx";
import {IAlbum} from "../interfaces";
import {loadAlbumData} from "../utils/dataActions";
import {RootStore} from "./rootStore";

export class AlbumStore {
    private rootStore: RootStore;

    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    @observable albums: null | IAlbum[];
    @observable totalAlbums: number = 0;
    @observable albumId: React.ChangeEvent<HTMLInputElement> | string;

    @action
    loadAlbums = async () => {
        this.albums = await loadAlbumData()
        this.totalAlbums = this.albums.length;
    }

    @action
    changeAlbum = (id: React.ChangeEvent<HTMLInputElement> | string) => {
        this.albumId = id;
    }
}

export default AlbumStore;