import {action, computed, observable} from "mobx";
import {loadPhotoData} from "../utils/dataActions";
import {RootStore} from "./rootStore";
import {pageSize} from "../utils/variables";

export class PagingStore {
    private rootStore: RootStore;

    constructor(rootStore) {
        this.rootStore = rootStore;
        this.calculateTotal()
    }

    @observable pageSize: number = 10;
    @observable currentPage: number = 1;
    @observable total: number = 0;

    @action
    setTotalCount(total = 0) {
        this.total = total;
    }

    @computed get pagesCount() : number {
        const fixed = (this.total / pageSize).toFixed()
        return +fixed - 1;
    }

    @computed get start() {
        return this.pageSize * this.currentPage;
    }

    @computed get limit() {
        return this.start + this.pageSize;
    }

    @action
    changePage(newPage: number) {
        this.currentPage = newPage;
    }

    @action
    async calculateTotal() {
        let data = []
        try {
            data = await loadPhotoData(0, -1);
        } catch (error) {
            console.log("-> error calculateTotal", error);
        }

        this.total = data.length;
    }
}

export default PagingStore;