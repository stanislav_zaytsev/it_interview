import {action, observable} from "mobx";
import {IPhoto} from "../interfaces";
import {loadPhotoData, removePhoto} from "../utils/dataActions";
import {RootStore} from "./rootStore";

export class PhotosStore {
    private rootStore: RootStore;

    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    @observable photos: null | IPhoto[];
    @observable isLoading: boolean;
    @observable removedPhotos: number[] = [];

    @action
    filterPhotosByAlbumId = () => {
        const {albumStore: {albumId}} = this.rootStore;
        this.photos = this.photos.filter(p => p.id === albumId)
    }

    @action
    loadPhotos = async () => {
        this.isLoading = true;
        const {pagingStore} = this.rootStore;
        this.photos = await loadPhotoData(pagingStore.start, pagingStore.pageSize
        )
        this.isLoading = false;
    }


    @action
    removePhoto = async (id:number) => {
        this.isLoading = true;

        await removePhoto(id);
        await this.loadPhotos();

        this.removedPhotos.push(id);
        this.isLoading = false;
    }

}

export default PhotosStore;