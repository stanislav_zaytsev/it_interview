import PhotosStore from "./photosStore";
import PagingStore from "./pagingStore";
import ModalStore from "./modalStore";
import AlbumStore from "./albumStore";

export class RootStore {
  public pagingStore: PagingStore;
  public photosStore: PhotosStore;
  public modalStore: ModalStore;
  public albumStore: AlbumStore;

  constructor() {
    this.pagingStore = new PagingStore(this);
    this.photosStore = new PhotosStore(this);
    this.modalStore = new ModalStore(this);
    this.albumStore = new AlbumStore(this);
  }
}