import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {RootStore} from "./stores/rootStore";
import {Provider} from "mobx-react";

ReactDOM.render(
    <Provider rootStore={new RootStore()} >
        <App/>
    </Provider>,
    document.getElementById('root')
);
