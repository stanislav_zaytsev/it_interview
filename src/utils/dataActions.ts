import {serverPath} from "./variables";

export async function loadPhotoData(start = 0, limit?: number) {
    let endPoint = `${serverPath}/photos?_start=${start}`;

    if (limit) endPoint+=`&_limit=${limit}`

    try {
        return await fetch(endPoint)
            .then((res) => res.json());
    } catch (e) {
        return null;
    }
}

export async function removePhoto(id: number) {
    await fetch(`${serverPath}/photos/${id}`, {
        method: 'DELETE',
    }).then(response => response.json())
}

export async function loadAlbumData() {
    let endPoint = `${serverPath}/albums`;

    try {
        return await fetch(endPoint)
            .then((res) => res.json());
    } catch (e) {
        return null;
    }
}
