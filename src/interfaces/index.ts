import {RootStore} from "../stores/rootStore";

export interface IPhoto {
    "albumId": string;
    "id": string;
    "title": string;
    "url": string;
    "thumbnailUrl": string;
}

export interface IAlbum {
    "id": string;
    "title": string;
}


export interface IInjectedProps {
    rootStore: RootStore;
}